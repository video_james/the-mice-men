// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TheMiceMenBlock.h"
#include "TheMiceMenBlockGrid.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstance.h"

ATheMiceMenBlock::ATheMiceMenBlock()
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		ConstructorHelpers::FObjectFinderOptional<UMaterial> BaseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> CheeseMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> BlueMaterial;
		ConstructorHelpers::FObjectFinderOptional<UMaterialInstance> RedMaterial;
		FConstructorStatics()
			: PlaneMesh(TEXT("/Game/Puzzle/Meshes/PuzzleCube.PuzzleCube"))
			, BaseMaterial(TEXT("/Game/Puzzle/Meshes/BaseMaterial.BaseMaterial"))
			, CheeseMaterial(TEXT("/Game/Puzzle/Meshes/CheeseMaterial.CheeseMaterial"))
			, BlueMaterial(TEXT("/Game/Puzzle/Meshes/BlueMaterial.BlueMaterial"))
			, RedMaterial(TEXT("/Game/Puzzle/Meshes/RedMaterial.RedMaterial"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create dummy root scene component
	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	// Save a pointer to each material
	BaseMaterial = ConstructorStatics.BaseMaterial.Get();
	CheeseMaterial = ConstructorStatics.CheeseMaterial.Get();
	BlueMaterial = ConstructorStatics.BlueMaterial.Get();
	RedMaterial = ConstructorStatics.RedMaterial.Get();

	// Create static mesh component
	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh0"));
	BlockMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	BlockMesh->SetRelativeScale3D(FVector(1.f,1.f,1.f));
	BlockMesh->SetRelativeLocation(FVector(0.f,0.f,0.5f));
	BlockMesh->SetMaterial(0, CheeseMaterial);
	BlockMesh->SetupAttachment(DummyRoot);
}


void ATheMiceMenBlock::SetTeam(ETeam::Type NewTeam)
{
	Team = NewTeam;
	BlockMesh->SetMaterial(0, NewTeam == ETeam::Blue ? BlueMaterial : RedMaterial);
}

void ATheMiceMenBlock::SetBlockType(EBlock::Type NewBlockType)
{
	BlockType = NewBlockType;
	if (BlockType == EBlock::Cheese)
	{
		BlockMesh->SetMaterial(0, CheeseMaterial);
	}	
}


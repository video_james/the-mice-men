// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TheMiceMenBlock.generated.h"

UENUM()
namespace EBlock
{	
	enum Type
	{
		Empty,
		Cheese,
		Mouse
	};
}
UENUM()
namespace ETeam
{
	enum Type
	{
		Blue,
		Red
	};
}

/** A block that can be clicked */
UCLASS(minimalapi)
class ATheMiceMenBlock : public AActor
{
	GENERATED_BODY()

	/** Dummy root component */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

	/** StaticMesh component for the clickable block */
	UPROPERTY(Category = Block, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* BlockMesh;

public:
	ATheMiceMenBlock();

	/** Pointer to white material used on the focused block */
	UPROPERTY()
	class UMaterial* BaseMaterial;

	/** Pointer to blue material used on inactive blocks */
	UPROPERTY()
	class UMaterialInstance* CheeseMaterial;

	UPROPERTY()
	class UMaterialInstance* BlueMaterial;

	UPROPERTY()
	class UMaterialInstance* RedMaterial;	

public:
	/** Returns DummyRoot subobject **/
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }
	/** Returns BlockMesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
	ETeam::Type Team;
	EBlock::Type BlockType;


public:
	void SetTeam(ETeam::Type Team);
	void SetBlockType(EBlock::Type Block);
};




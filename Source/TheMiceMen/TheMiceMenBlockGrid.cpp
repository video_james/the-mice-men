// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TheMiceMenBlockGrid.h"
#include "TheMiceMenBlock.h"
#include "Components/TextRenderComponent.h"
#include "Engine/World.h"
#include "Math/UnrealMathUtility.h"

#define LOCTEXT_NAMESPACE "PuzzleBlockGrid"

int m_MouseCountBlue;
int m_MouseCountRed;

ATheMiceMenBlockGrid::ATheMiceMenBlockGrid()
{
	PrimaryActorTick.bCanEverTick = true;	

	DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;
}

void ATheMiceMenBlockGrid::BeginPlay()
{
	Super::BeginPlay();	

	Offset = FVector(-Height * 0.5f * BlockSpacing, -Width * 0.5f * BlockSpacing, 0.f) + GetActorLocation();
	SpawnColumnIndicators();

	StartNewGame();
}

void ATheMiceMenBlockGrid::StartNewGame()
{
	MovingMouse = nullptr;

	SelectedColumnIndex = 0;
	LastColumnMovedBlue = -1;
	LastColumnMovedConsecutivelyBlue = 0;
	LastColumnMovedRed = -1;
	LastColumnMovedConsecutivelyRed = 0;

	CurrentTeam = FMath::RandRange(0, 1) == 0 ? ETeam::Blue : ETeam::Red;
	OtherTeam = CurrentTeam == ETeam::Blue ? ETeam::Red : ETeam::Blue;

	RemainingMiceBlue = MaxMicePerTeam;
	RemainingMiceRed = MaxMicePerTeam;

	ClearBoard();
	SpawnMice(ETeam::Blue, &m_MouseCountBlue);
	SpawnMice(ETeam::Red, &m_MouseCountRed);
	BlockMiceMovement();
	FillGridWithCheeses();

	IsTurnTimerInEffect = false;
	IsGamePlaying = true;
}

void ATheMiceMenBlockGrid::ClearBoard()
{
	for (int32 i = 0; i < Blocks.Num(); i++)
	{
		if (Blocks[i] != nullptr)
		{
			Blocks[i]->Destroy();
		}		
	}
	Blocks.Reset(Width * Height);
	Blocks.AddZeroed(Width * Height);
}

void ATheMiceMenBlockGrid::SpawnColumnIndicators()
{
	ColumnIndicators.Reset(Width);
	ColumnIndicators.AddZeroed(Width);
	for (int32 i = 0; i < Width; i++)
	{
		const float xPosition = i * BlockSpacing + BlockSpacing * 0.5f;
		const float yPosition = -1 * BlockSpacing + BlockSpacing * 0.5f;
		const FVector SpawnLocation = Offset + FVector(yPosition, xPosition, 0.f);
		ATheMiceMenBlock* NewBlock = GetWorld()->SpawnActor<ATheMiceMenBlock>();
		NewBlock->SetActorLocation(SpawnLocation, false);		
		ColumnIndicators[i] = NewBlock;
		SetColumnAvailable(i, false);
	}
}

void ATheMiceMenBlockGrid::SpawnMice(ETeam::Type team, int32* mouseCount)
{
	const int direction = team == ETeam::Blue ? 1 : -1;
	const int columnCount = Width / 2;
	const int middleColumn = Width % 2 == 1 ? columnCount + 1 : columnCount;
	const int maxMicePerColumn = 4;

	int i = 0;
	*mouseCount = 0;
	while (*mouseCount < MaxMicePerTeam)
	{
		int columnIndex = columnCount - direction * i - direction;
		if (GetBlockCountInColumn(columnIndex, EBlock::Mouse, false, team) < maxMicePerColumn)
		{
			bool spawnInThisColumn = FMath::RandRange(0, 2) == 0;
			if (spawnInThisColumn)
			{
				int rowIndex = FMath::RandRange(0, Height - 1);
				if (IsPositionEmpty(columnIndex, rowIndex))
				{
					ATheMiceMenBlock* NewBlock = AddBlock(columnIndex, rowIndex);
					if (NewBlock != nullptr)
					{
						NewBlock->SetBlockType(EBlock::Mouse);
						NewBlock->SetTeam(team);						
						(*mouseCount)++;
					}
				}
			}
		}
		i = (i + 1) % columnCount;
	}
}

void ATheMiceMenBlockGrid::BlockMiceMovement()
{
	for (int32 i = 0; i < Width; i++)
	{
		for (int32 j = 0; j < Height; j++)
		{
			ATheMiceMenBlock* Block = GetBlock(i, j);
			if (Block != nullptr && Block->BlockType == EBlock::Mouse)
			{
				int32 direction = Block->Team == ETeam::Blue ? 1 : -1;
				BlockMouseMovementWithCheese(direction, i, j);
			}			
		}
	}	
}

void ATheMiceMenBlockGrid::BlockMouseMovementWithCheese(int32 XDirection, int32 XIndex, int32 YIndex)
{
	int32 XNewIndex = XIndex + XDirection;
	if (IsPositionEmpty(XNewIndex, YIndex))
	{
		ATheMiceMenBlock* NewBlock = AddBlock(XNewIndex, YIndex);
		NewBlock->SetBlockType(EBlock::Cheese);
	}

	int32 YNewIndex = YIndex - 1;
	WrapNumber(&YNewIndex, Height);
	if (IsPositionEmpty(XIndex, YNewIndex))
	{
		ATheMiceMenBlock* NewBlock = AddBlock(XIndex, YNewIndex);
		NewBlock->SetBlockType(EBlock::Cheese);
	}
}

void ATheMiceMenBlockGrid::FillGridWithCheeses()
{
	int32 CheeseInColumn = 0;
	for (int32 i = 0; i < Width; i++)
	{
		CheeseInColumn = GetBlockCountInColumn(i, EBlock::Cheese, false, ETeam::Blue);
		while (CheeseInColumn < MinCheese)
		{
			for (int32 j = 0; j < Height; j++)
			{
				if (IsPositionEmpty(i, j))
				{
					bool spawnHere = FMath::RandRange(0, 2) == 0;
					if (spawnHere)
					{
						ATheMiceMenBlock* NewBlock = AddBlock(i, j);
						if (NewBlock != nullptr)
						{
							NewBlock->SetBlockType(EBlock::Cheese);
							CheeseInColumn++;
						}
					}
				}
				if (CheeseInColumn >= MaxCheese)
				{
					break;
				}
			}
		}
	}
}

ATheMiceMenBlock* ATheMiceMenBlockGrid::AddBlock(int32 XIndex, int32 YIndex)
{
	int32 flattenedIndex = GetFlattenedIndex(XIndex, YIndex);

	if (flattenedIndex >= 0 && flattenedIndex < Blocks.Num())
	{
		ATheMiceMenBlock* NewBlock = GetWorld()->SpawnActor<ATheMiceMenBlock>();
		SetBlock(NewBlock, XIndex, YIndex);
		return NewBlock;
	}
	return nullptr;
}

ATheMiceMenBlock* ATheMiceMenBlockGrid::GetBlock(int32 XIndex, int32 YIndex)
{
	return GetBlock(GetFlattenedIndex(XIndex, YIndex));
}

ATheMiceMenBlock* ATheMiceMenBlockGrid::GetBlock(int32 flattenedIndex)
{
	ATheMiceMenBlock* block = nullptr;
	if (flattenedIndex >= 0 && flattenedIndex < Blocks.Num())
	{
		block = Blocks[flattenedIndex];
	}
	return block;
}

void ATheMiceMenBlockGrid::SetBlock(ATheMiceMenBlock* Block, int32 XIndex, int32 YIndex)
{	
	int32 flattenedIndex = GetFlattenedIndex(XIndex, YIndex);
	if (flattenedIndex >= 0 && flattenedIndex < Blocks.Num())
	{
		if (Block != nullptr)
		{			
			const float xPosition = XIndex * BlockSpacing + BlockSpacing * 0.5f;
			const float yPosition = YIndex * BlockSpacing + BlockSpacing * 0.5f;
			const FVector BlockLocation = Offset + FVector(yPosition, xPosition, 0.f);
			Block->SetActorLocation(BlockLocation, false);
		}
		Blocks[flattenedIndex] = Block;
	}
}

int32 ATheMiceMenBlockGrid::GetFlattenedIndex(int32 XIndex, int32 YIndex)
{
	WrapNumber(&YIndex, Height);
	return XIndex * Height + YIndex;
}

bool ATheMiceMenBlockGrid::GetGridAddressWithOffset(int32 XInitialindex, int32 YInitialIndex, int32 XOffset, int32 YOffset, int32* XReturnIndex, int32* YReturnIndex)
{
	int32 XNewIndex = XInitialindex + XOffset;
	int32 YNewIndex = YInitialIndex + YOffset;

	if (XNewIndex < 0 || XNewIndex >= Width)
	{
		return false;
	}

	WrapNumber(&YNewIndex, Height);
	*XReturnIndex = XNewIndex;
	*YReturnIndex = YNewIndex;

	return true;
}

bool ATheMiceMenBlockGrid::IsPositionEmpty(int32 XIndex, int32 YIndex)
{
	return GetBlock(XIndex, YIndex) == nullptr;
}

int32 ATheMiceMenBlockGrid::GetBlockCountInColumn(int32 ColumnIndex, EBlock::Type BlockType, bool MustMatchTeam, ETeam::Type Team)
{
	int32 count = 0;
	for (int32 i = 0; i < Height; i++)
	{
		ATheMiceMenBlock* block = GetBlock(ColumnIndex, i);
		if (block != nullptr && block->BlockType == BlockType && block->Team == Team)
		{
			count++;
		}
	}
	return count;
}

void ATheMiceMenBlockGrid::WrapNumber(int32* number, int32 maxNumber)
{
	while (*number < 0)
	{
		*number += maxNumber;
	}
	*number %= maxNumber;
}

int32 ATheMiceMenBlockGrid::GetMouseCount(ETeam::Type Team)
{
	int32 Count = 0;
	for (int i = 0; i < Width; i++)
	{
		Count += GetBlockCountInColumn(i, EBlock::Mouse, true, Team);
	}
	return Count;
}

void ATheMiceMenBlockGrid::ShowAvailableColumns()
{
	AvailableColumns.Reset();
		
	int32 Direction = CurrentTeam == ETeam::Blue ? 1 : -1;
	int32 StartIndex = Direction == 1 ? Width - 1 : 0;

	int32 FirstValidIndex = -1;

	for (int32 i = 0; i < Width; i++)
	{
		int32 ColumnIndex = i * -Direction + StartIndex;
		bool IsColumnAvailable = false;

		bool DoesColumnContainTeamMice = GetBlockCountInColumn(ColumnIndex, EBlock::Mouse, true, CurrentTeam) > 0;
		if (DoesColumnContainTeamMice)
		{
			if (FirstValidIndex == -1)
			{
				FirstValidIndex = i;
			}

			bool IsColumnBlackListed = UnavailableColumns.Contains(ColumnIndex);
			if (!IsColumnBlackListed)
			{				
				
				bool hasColumnBeenMovedTooManyTimes = false;
				if (CurrentTeam == ETeam::Blue)
				{
					hasColumnBeenMovedTooManyTimes = LastColumnMovedBlue == ColumnIndex && LastColumnMovedConsecutivelyBlue >= MaxConsecutiveColumnMoves;
				}
				else
				{
					hasColumnBeenMovedTooManyTimes = LastColumnMovedRed == ColumnIndex && LastColumnMovedConsecutivelyRed >= MaxConsecutiveColumnMoves;
				}

				if (!hasColumnBeenMovedTooManyTimes)
				{
					AvailableColumns.Add(ColumnIndex);
					IsColumnAvailable = true;
				}
			}
		}
		
		SetColumnAvailable(ColumnIndex, IsColumnAvailable);
	}	

	if (AvailableColumns.Num() == 0)
	{
		AvailableColumns.Add(FirstValidIndex);
		SetColumnAvailable(FirstValidIndex, true);
	}
	SelectedColumnIndex = 0;
	SetSelectedColumnHighlight(true);

	UnavailableColumns.Empty();
}

void ATheMiceMenBlockGrid::SetSelectedColumnHighlight(bool isHighlighted)
{
	if (SelectedColumnIndex < 0 || SelectedColumnIndex >= AvailableColumns.Num())
	{
		return;
	}
	int32 index = AvailableColumns[SelectedColumnIndex];
	if (index < 0 || index >= ColumnIndicators.Num())
	{
		return;
	}
	if (isHighlighted)
	{
		ColumnIndicators[index]->SetTeam(CurrentTeam);
	}
	else
	{
		ColumnIndicators[index]->SetBlockType(EBlock::Cheese);
	}
}

void ATheMiceMenBlockGrid::SetColumnAvailable(int32 ColumnIndex, bool IsAvailable)
{
	ColumnIndicators[ColumnIndex]->SetActorScale3D(
		IsAvailable ?
		FVector(0.5f, 0.5f, 0.5f) :
		FVector(0.1f, 0.1f, 0.1f)
	);
	ColumnIndicators[ColumnIndex]->SetBlockType(EBlock::Cheese);	
}

void ATheMiceMenBlockGrid::SelectNextColumn(int32 Direction)
{	
	Direction *= CurrentTeam == ETeam::Blue ? -1 : 1;
	SetSelectedColumnHighlight(false);
	SelectedColumnIndex += Direction;
	WrapNumber(&SelectedColumnIndex, AvailableColumns.Num());
	SetSelectedColumnHighlight(true);
}

void ATheMiceMenBlockGrid::MoveSelectedColumn(int32 Direction)
{
	TArray<ATheMiceMenBlock*> OldBlocks = TArray<ATheMiceMenBlock*>(Blocks);
	int32 ColumnIndex = AvailableColumns[SelectedColumnIndex];

	if (CurrentTeam == ETeam::Blue)
	{
		if (LastColumnMovedBlue != ColumnIndex)
		{
			LastColumnMovedConsecutivelyBlue = 0;
			LastColumnMovedBlue = ColumnIndex;
		}
		LastColumnMovedConsecutivelyBlue++;
	}
	else if (CurrentTeam == ETeam::Red)
	{
		if (LastColumnMovedRed != ColumnIndex)
		{
			LastColumnMovedConsecutivelyRed = 0;
			LastColumnMovedRed = ColumnIndex;
		}
		LastColumnMovedConsecutivelyRed++;
	}

	for (int32 i = 0; i < Height; i++)
	{
		int32 YLastBlockIndex = i - Direction;
		WrapNumber(&YLastBlockIndex, Height);
		ATheMiceMenBlock* LastBlock = OldBlocks[GetFlattenedIndex(ColumnIndex, YLastBlockIndex)];
		SetBlock(LastBlock, ColumnIndex, i);
	}
}

bool ATheMiceMenBlockGrid::MoveNextMouse()
{
	ETeam::Type TeamMoving = CurrentTeam;	
	int32 TeamsChecked = 0;

	while (TeamsChecked < 2)
	{		
		int32 Direction = TeamMoving == ETeam::Blue ? 1 : -1;
		int32 StartIndex = Direction == 1 ? Width - 1 : 0;		
		
		if (MovingMouse != nullptr)
		{
			TeamMoving = MovingMouse->Team;
			Direction = TeamMoving == ETeam::Blue ? 1 : -1;
			StartIndex = Direction == 1 ? Width - 1 : 0;
			int32 Distance = (XMovingMouseIndex / Direction) - StartIndex;
			
			if (MoveMouse(MovingMouse, Distance, Direction, XMovingMouseIndex, YMovingMouseIndex))
			{
				return true;
			}
			else
			{			
				MovingMouse = nullptr;
				if (TeamMoving == CurrentTeam)
				{
					UnavailableColumns.Add(XMovingMouseIndex);
				}
			}
		}

		for (int32 i = 0; i < Width; i++)
		{
			int32 ColumnIndex = i * -Direction + StartIndex;

			for (int32 j = 0; j < Height; j++)
			{	
				MovingMouse = GetBlock(ColumnIndex, j);
				if (MovingMouse != nullptr && MovingMouse->BlockType == EBlock::Mouse && MovingMouse->Team == TeamMoving)
				{
					if (MoveMouse(MovingMouse, i, Direction, ColumnIndex, j))
					{
						return true;
					}					
				}				
			}
		}
		MovingMouse = nullptr;
		TeamMoving = TeamMoving == ETeam::Blue ? ETeam::Red : ETeam::Blue;
		TeamsChecked++;
	}

	if (IsTurnTimerInEffect)
	{
		if (CurrentTeam == ETeam::Blue)
		{
			TurnTimerBlue--;
		}
		else
		{
			TurnTimerRed--;
		}
		if (TurnTimerBlue <= 0 && TurnTimerRed <= 0)
		{
			ShowDraw();
			return true;
		}
	}
	else
	{
		if (GetMouseCount(ETeam::Blue) == 1 && GetMouseCount(ETeam::Red) == 1)
		{
			IsTurnTimerInEffect = true;
			TurnTimerBlue = TurnTimer;
			TurnTimerRed = TurnTimer;
		}
	}

	CurrentTeam = CurrentTeam == ETeam::Blue ? ETeam::Red : ETeam::Blue;
	OtherTeam = OtherTeam == ETeam::Blue ? ETeam::Red : ETeam::Blue;	

	return false;
}

bool ATheMiceMenBlockGrid::MoveMouse(ATheMiceMenBlock* Mouse, int32 XDistanceFromTarget, int32 Direction, int32 XIndex, int32 YIndex)
{
	if (XDistanceFromTarget == 0)
	{
		EjectMouse(Mouse, Direction, XIndex, YIndex);
		SetBlock(nullptr, XIndex, YIndex);
		return true;
	}

	int32 YBelowIndex = YIndex - 1;
	WrapNumber(&YBelowIndex, Height);
	if (IsPositionEmpty(XIndex, YBelowIndex))
	{
		SetBlock(Mouse, XIndex, YBelowIndex);
		SetBlock(nullptr, XIndex, YIndex);
		XMovingMouseIndex = XIndex;
		YMovingMouseIndex = YBelowIndex;
		return true;
	}

	int32 XInFrontIndex = XIndex + Direction;
	if (IsPositionEmpty(XInFrontIndex, YIndex))
	{
		SetBlock(Mouse, XInFrontIndex, YIndex);
		SetBlock(nullptr, XIndex, YIndex);
		XMovingMouseIndex = XInFrontIndex;
		YMovingMouseIndex = YIndex;
		return true;
	}

	return false;
}

void ATheMiceMenBlockGrid::EjectMouse(ATheMiceMenBlock* Mouse, int32 Direction, int32 XIndex, int32 YIndex)
{
	Mouse->Destroy();
	MovingMouse = nullptr;
	
	if (Mouse->Team == ETeam::Blue)
	{
		RemainingMiceBlue--;	
		if (RemainingMiceBlue == 0)
		{
			ShowWinner(ETeam::Blue);
		}
	}
	else if (Mouse->Team == ETeam::Red)
	{
		RemainingMiceRed--;
		if (RemainingMiceRed == 0)
		{
			ShowWinner(ETeam::Red);
		}
	}	
}

void ATheMiceMenBlockGrid::ShowWinner(ETeam::Type Team)
{
	IsGamePlaying = false;
	for (int32 i = 0; i < Width; i++)
	{
		ColumnIndicators[i]->SetActorScale3D(FVector(0.75f, 0.75f, 0.75f));
		ColumnIndicators[i]->SetTeam(Team);
	}
}

void ATheMiceMenBlockGrid::ShowDraw()
{
	IsGamePlaying = false;
	for (int32 i = 0; i < Width; i++)
	{
		ColumnIndicators[i]->SetActorScale3D(FVector(0.75f, 0.75f, 0.75f));
		ColumnIndicators[i]->SetBlockType(EBlock::Cheese);
	}
}

#undef LOCTEXT_NAMESPACE

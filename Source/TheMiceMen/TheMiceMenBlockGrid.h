// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "TheMiceMenBlock.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TheMiceMenBlockGrid.generated.h"


UCLASS()
class ATheMiceMenBlockGrid : public AActor
{
	GENERATED_BODY()

	UPROPERTY(Category = Grid, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* DummyRoot;

public:
	ATheMiceMenBlockGrid();

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Width;
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 Height;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 MinCheese;
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 MaxCheese;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 MaxMicePerTeam;

	UPROPERTY(Category=Grid, EditAnywhere, BlueprintReadOnly)
	float BlockSpacing;

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
	int32 MaxConsecutiveColumnMoves;	

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadWrite)
	int32 TurnTimer;
	

protected:
	virtual void BeginPlay() override;	
	UPROPERTY(Category = Grid, BlueprintReadOnly)
	bool IsGamePlaying;
	   
public:   
	FORCEINLINE class USceneComponent* GetDummyRoot() const { return DummyRoot; }

private:
	

	TArray<ATheMiceMenBlock*> Blocks;
	TArray<ATheMiceMenBlock*> ColumnIndicators;

	TArray<int32> AvailableColumns;
	TArray<int32> UnavailableColumns;
	int32 SelectedColumnIndex;
	int32 LastColumnMovedBlue;
	int32 LastColumnMovedConsecutivelyBlue;
	int32 LastColumnMovedRed;
	int32 LastColumnMovedConsecutivelyRed;

	FVector Offset;

	ETeam::Type CurrentTeam;
	ETeam::Type OtherTeam;

	int32 RemainingMiceBlue;
	int32 RemainingMiceRed;

	ATheMiceMenBlock* MovingMouse;
	int32 XMovingMouseIndex;
	int32 YMovingMouseIndex;

	int32 TurnTimerBlue;
	int32 TurnTimerRed;
	bool IsTurnTimerInEffect;

private:
	UFUNCTION(BlueprintCallable, Category = "Grid")
	void StartNewGame();
	void ClearBoard();
	void SpawnColumnIndicators();
	void SpawnMice(ETeam::Type Team, int32* MouseCount);
	void BlockMiceMovement();
	void BlockMouseMovementWithCheese(int32 XDirection, int32 XIndex, int32 YIndex);
	void FillGridWithCheeses();

	ATheMiceMenBlock* AddBlock(int32 XIndex, int32 YIndex);
	ATheMiceMenBlock* GetBlock(int32 XIndex, int32 YIndex);
	ATheMiceMenBlock* GetBlock(int32 flattenedIndex);
	void SetBlock(ATheMiceMenBlock*  Block, int32 XIndex, int32 YIndex);
	int32 GetFlattenedIndex(int32 XIndex, int32 YIndex);
	bool GetGridAddressWithOffset(int32 XInitialindex, int32 YInitialIndex, int32 XOffset, int32 YOffset, int32* XReturnIndex, int32* YReturnIndex);
	bool IsPositionEmpty(int32 XIndex, int32 YIndex);
	int32 GetBlockCountInColumn(int32 ColumnIndex, EBlock::Type BlockTypee, bool MustMatchTeam, ETeam::Type Team);
	void WrapNumber(int32* YIndex, int32 MaxNumber);
	int32 GetMouseCount(ETeam::Type Team);
	
	UFUNCTION(BlueprintCallable, Category = "Grid")
	void ShowAvailableColumns();
	void SetSelectedColumnHighlight(bool isHighlighted);
	void SetColumnAvailable(int32 ColumnIndex, bool IsAvailable);

	UFUNCTION(BlueprintCallable, Category = "Grid")
	void SelectNextColumn(int32 Direction);

	UFUNCTION(BlueprintCallable, Category = "Grid")
	void MoveSelectedColumn(int32 Direction);

	UFUNCTION(BlueprintCallable, Category = "Grid")
	bool MoveNextMouse();
	bool MoveMouse(ATheMiceMenBlock* Mouse, int32 Direction, int32 XDistanceFromTarget, int32 XIndex, int32 YIndex);
	void EjectMouse(ATheMiceMenBlock* Mouse, int32 Direction, int32 XIndex, int32 YIndex);

	void ShowWinner(ETeam::Type Team);
	void ShowDraw();
};
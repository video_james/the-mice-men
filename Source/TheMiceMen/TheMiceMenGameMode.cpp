// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TheMiceMenGameMode.h"
#include "TheMiceMenPlayerController.h"
#include "TheMiceMenPawn.h"

ATheMiceMenGameMode::ATheMiceMenGameMode()
{
	// no pawn by default
	DefaultPawnClass = ATheMiceMenPawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ATheMiceMenPlayerController::StaticClass();
}

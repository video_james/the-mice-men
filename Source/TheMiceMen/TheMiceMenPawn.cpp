// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TheMiceMenPawn.h"
#include "TheMiceMenBlock.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

ATheMiceMenPawn::ATheMiceMenPawn(const FObjectInitializer& ObjectInitializer) 
	: Super(ObjectInitializer)
{
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void ATheMiceMenPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (APlayerController* PC = Cast<APlayerController>(GetController()))
	{
		
	}
}

void ATheMiceMenPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	// TODO: Bind an action for keys selecting and moving columns
	//PlayerInputComponent->BindAction("ResetVR", EInputEvent::IE_Pressed, this, &ATheMiceMenPawn::OnResetVR);
}

void ATheMiceMenPawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

	OutResult.Rotation = FRotator(-90.0f, -90.0f, 0.0f);
}
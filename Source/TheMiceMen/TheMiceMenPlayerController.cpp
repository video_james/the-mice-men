// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TheMiceMenPlayerController.h"

ATheMiceMenPlayerController::ATheMiceMenPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
